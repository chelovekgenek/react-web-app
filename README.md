# Requirement installations
- [NodeJS](https://nodejs.org/en/)

## Running on development
- unpack from archive of clone from repository
- change directory to project folder
- ```npm install```
- configure configuration file in project_dir/config/default.json
- configure auth0 variables in project_dir/src/constants/index.js
- ```npm start```

## Running on production
- unpack from archive of clone from repository
- change directory to project folder
- ```set NODE_ENV=production```
- ```npm install -g webpack```
- ```npm install```
- ```webpack -p --config ./webpack.production.config.js```
- configure configuration file in project_dir/config/production.json
- configure auth0 variables in project_dir/src/constants/index.js
- ```npm start```