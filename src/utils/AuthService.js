import Auth0Lock from 'auth0-lock'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { completeLogin, completeLogout} from '../actions/app'

export default class AuthService {
  constructor(clientId, domain, dispatch) {
    // Configure Auth0
    this.lock = new Auth0Lock(clientId, domain, {
      auth: {
        redirect: false,
        responseType: 'token',
        params: {}
      },
      theme: {
        labeledSubmitButton: false
      }
    })
    // Add callback for lock `authenticated` event
    this.dispatchToStore = dispatch
    this.lock.on('authenticated', ::this._doAuthentication)
    // binds login functions to keep this context
    this.showWidget = ::this.showWidget
    this.logout = ::this.logout
  }

  _doAuthentication(authResult) {
    console.log('authenticated', authResult)
    // Saves the user token
    this.setToken(authResult.idToken)
    this.login()
  }

  showWidget() {
    // Call the show method to display the widget.
    this.lock.show()
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    return !!this.getToken()
  }

  setToken(idToken) {
    // Saves user token to local storage
    localStorage.setItem('id_token', idToken)
  }

  getToken() {
    // Retrieves the user token from local storage
    return localStorage.getItem('id_token')
  }

  login() {
    if (this.loggedIn())
      this.dispatchToStore(completeLogin())
  }
  logout() {
    // Clear user token and profile data from local storage
    localStorage.removeItem('id_token');
    if (!this.loggedIn())
      this.dispatchToStore(completeLogout())
  }
}
