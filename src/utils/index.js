import React from 'react'
import jwtDecoder from 'jwt-decode'

import { AUTH0_CLIENT_ID, STATUSES } from '../constants/'
import * as Style from '../constants/style'

import MenuItem from 'material-ui/MenuItem'
import {
  Step,
  Stepper,
  StepButton,
} from 'material-ui/Stepper'
export function getStatusMenuItems() {
  let items = []
  STATUSES.forEach((item, i) => items.push(
    <MenuItem key={i} value={item.value} primaryText={item.label} />
  ))
  return items
}

export function getWaistSteps(waist) {
  let steps = []
  Object.keys(waist).forEach((item, i) => steps.push(
    <Step key={i} style={Style.step} completed={true}>
      <StepButton className='step-icon' />
    </Step>
  ))
  if (steps.length < 4) steps.push(
    <Step key={steps.length} style={Style.step} active={true}>
      <StepButton className='step-icon' />
    </Step>
  )
  for(let i = steps.length; i < 4; i++) {
    steps.push(
      <Step key={steps.length} style={Style.step}>
        <StepButton className='step-icon' />
      </Step>
    )
  }
  return steps
}

export function isDatesEqual(date1, date2) {
  let verD1 = verifyDate(date1),
      verD2 = verifyDate(date2)

  if (verD1 && verD2 && verD1.getTime() === verD2.getTime())
      return true
  else
    return false
}
export function isSubstring(str, substr) {
  if (!str || !substr || typeof substr !== 'string')
    return false

  if (str.toLowerCase().indexOf(substr.toLowerCase()) === -1)
    return false
  else
    return true
}
function verifyDate(date) {
  let verified
  if (isEmpty(date))  return null
  if (!(date instanceof Date))
    verified = new Date(date)
  else return date
  if (verified === 'undefined' || verified.toString() === 'Invalid Date')
    return null
  else
    return verified
}

var hasOwnProperty = Object.prototype.hasOwnProperty
export function isEmpty(obj) {
    if (obj == null) return true;

    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    if (typeof obj !== 'object') return true;
    if (obj instanceof Date) return false

    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

export function isAuthenticated(token)  {
  try {
    let decoded = jwtDecoder(token)
    if (decoded.aud === AUTH0_CLIENT_ID && Math.floor(Date.now() / 1000) < decoded.exp)
      return true
  } catch(ignore) { }
  return false
}

export function clearRoute(location) {
  return location.replace('/', '')
}
