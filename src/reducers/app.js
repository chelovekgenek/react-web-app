import {
  APP_SET_AUTH_ENVIRONMENT,
  APP_COMPLETE_LOGIN,
  APP_COMPLETE_LOGOUT
 } from '../constants/'


const initialState = {
  auth: {},
  isAuthenticated: false
}

export default function app(state = initialState, action) {
  switch (action.type) {
    case APP_SET_AUTH_ENVIRONMENT:
      return { ...state, auth: action.payload }
    case APP_COMPLETE_LOGIN:
      return { ...state, isAuthenticated: true }
    case APP_COMPLETE_LOGOUT:
      return { ...state, isAuthenticated: false }
    default:
      return state
  }
}
