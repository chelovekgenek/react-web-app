import { combineReducers } from 'redux'

import app from './app'
import clients from './clients'

const rootReducer =  combineReducers({
  app,
  clients
})

export default rootReducer
