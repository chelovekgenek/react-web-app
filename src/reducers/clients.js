import {
  CLIENTS_CLEAR,
  CLIENTS_SELECT,
  CLIENTS_LOAD_ALL_REQUEST,
  CLIENTS_LOAD_ALL_SUCCESS,
  CLIENTS_LOAD_ALL_ERROR,
  CLIENTS_INSERT_NEW_REQUEST,
  CLIENTS_INSERT_NEW_SUCCESS,
  CLIENTS_INSERT_NEW_ERROR,
  CLIENTS_UPDATE_NEW_REQUEST,
  CLIENTS_UPDATE_NEW_SUCCESS,
  CLIENTS_UPDATE_NEW_ERROR
 } from '../constants/'


const initialState = {
  data: [],
  client: {},
  fetching: false,
  error: false
}

export default function(state = initialState, action) {
  let data
  switch (action.type) {
    case CLIENTS_LOAD_ALL_REQUEST:
      return { ...state, fetching: true }
    case CLIENTS_LOAD_ALL_SUCCESS:
      return { ...state, data: action.payload, fetching: false }
    case CLIENTS_LOAD_ALL_ERROR:
      return { ...state, error: action.payload, fetching: false }
    case CLIENTS_INSERT_NEW_REQUEST:
      return { ...state, fetching: true }
    case CLIENTS_INSERT_NEW_SUCCESS:
      data = state.data
      data.push(action.payload)
      return { ...state, data, fetching: false }
    case CLIENTS_INSERT_NEW_ERROR:
      return { ...state, error: action.payload, fetching: false }
    case CLIENTS_UPDATE_NEW_REQUEST:
      return { ...state, fetching: true }
    case CLIENTS_UPDATE_NEW_SUCCESS:
      data = state.data.map(item => {
        if (item.id === action.payload.id)
          return action.payload
        else
          return item
      })
      return { ...state, data, client: action.payload, fetching: false }
    case CLIENTS_UPDATE_NEW_ERROR:
      return { ...state, error: action.payload, fetching: false }
    case CLIENTS_SELECT:
      state.data.forEach(item => {
        if (item.id === action.payload) data = item
      })
      if (data)
        return { ...state, client: data }
      else
        return state
    case CLIENTS_CLEAR:
      return {...state, data: [], client: {}, fetching: false, error: false }
    default:
      return state
  }
}
