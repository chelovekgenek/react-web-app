import React, { Component } from 'react'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import Header from '../components/Header'
import TopPanel from '../components/TopPanel'
import Waist from '../components/Waist'

export default class ClientInfo extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <Header routes={this.props.routes}/>
          <TopPanel />
          <Waist />
        </div>
      </MuiThemeProvider>
    )
  }
}
