import React, { Component } from 'react'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import Header from '../components/Header'
import TopPanel from '../components/TopPanel'
import List from '../components/List'

export default class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <Header routes={this.props.routes}/>
          <TopPanel />
          <List />
        </div>
      </MuiThemeProvider>
    )
  }
}
