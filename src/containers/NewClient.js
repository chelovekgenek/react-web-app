import React, { Component } from 'react'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import Header from '../components/Header'
import TopPanel from '../components/TopPanel'
import New from '../components/New'


export default class NewClient extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <Header routes={this.props.routes}/>
          <TopPanel />
          <New />
        </div>
      </MuiThemeProvider>
    )
  }
}
