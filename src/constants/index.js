export const AUTH0_DOMAIN         = 'react-toolbox-emiliano-evghenii.eu.auth0.com'
export const AUTH0_CLIENT_ID      = 'QZ8kX6nTjeM3vlxHrHhH8tokHHbyJWE6'
export const AUTH0_CLIENT_SECRET  = 'ZFRlcDwu_5pee3RrDvq3efMauNLEQvR5qVNrfOgpguFKJf3eKUoNJue78VnfUabo'

//export const JSON_SERVER_HOST     = 'https://shrouded-thicket-38889.herokuapp.com/db'
export const JSON_SERVER_HOST_DB      = 'http://' + window.location.hostname + ':3000/db'
export const JSON_SERVER_HOST_CLIENTS = 'http://' + window.location.hostname + ':3000/clients'

export const PROMISE = 'PROMISE'
export const REDIRECT = 'REDIRECT'

export const APP_SET_AUTH_ENVIRONMENT = 'APP_SET_AUTH_ENVIRONMENT'
export const APP_COMPLETE_LOGIN       = 'APP_COMPLETE_LOGIN'
export const APP_COMPLETE_LOGOUT      = 'APP_COMPLETE_LOGOUT'

export const STATUS_COMPLETED   = 'completed'
export const STATUS_ACTIVE      = 'active'
export const STATUS_UNAVAILABLE = 'unvailable'

export const CLIENTS_LOAD_ALL_REQUEST   = 'CLIENTS_LOAD_ALL_REQUEST'
export const CLIENTS_LOAD_ALL_SUCCESS   = 'CLIENTS_LOAD_ALL_SUCCESS'
export const CLIENTS_LOAD_ALL_ERROR     = 'CLIENTS_LOAD_ALL_ERROR'
export const CLIENTS_SELECT             = 'CLIENTS_SELECT'
export const CLIENTS_CLEAR              = 'CLIENTS_CLEAR'
export const CLIENTS_INSERT_NEW_REQUEST = 'CLIENTS_INSERT_NEW_REQUEST'
export const CLIENTS_INSERT_NEW_SUCCESS = 'CLIENTS_INSERT_NEW_SUCCESS'
export const CLIENTS_INSERT_NEW_ERROR   = 'CLIENTS_INSERT_NEW_ERROR'
export const CLIENTS_UPDATE_NEW_REQUEST = 'CLIENTS_UPDATE_NEW_REQUEST'
export const CLIENTS_UPDATE_NEW_SUCCESS = 'CLIENTS_UPDATE_NEW_SUCCESS'
export const CLIENTS_UPDATE_NEW_ERROR   = 'CLIENTS_UPDATE_NEW_ERROR'

export const STATUSES = [
  {label: 'Active',   value: 'active'},
  {label: 'Done',     value: 'done'},
  {label: 'Stopped',  value: 'stopped'},
  {label: 'Delayed',  value: 'delayed'},
  {label: 'Paused',   value: 'paused'},
]
export const STATUSES_FULL = [
  {label: 'All',      value: 'all'},
  {label: 'Active',   value: 'active'},
  {label: 'Done',     value: 'done'},
  {label: 'Stopped',  value: 'stopped'},
  {label: 'Delayed',  value: 'delayed'},
  {label: 'Paused',   value: 'paused'},
]
