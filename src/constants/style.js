export const row = {
  margin: 0,
}
export const left = {
  float: 'left'
}
export const right = {
  float: 'right'
}
export const rowTitle = {
  margin: 0,
  marginTop: 20,
  fontFamily: '\'Roboto Bold\', \'Roboto\'',
  fontSize: 18,
  fontWeight: 700
}
export const text = {
  margin: 20,
  fontFamily: 'Roboto Bold, Roboto',
  fontSize: 15,
  fontWeight: 500
}

export const textNumber = {
  color: '#5CB85C'
}
export const textCommon = {
  color: '#282623'
}
export const input = {
  marginLeft: 20
}

export const rowStep = {...row, width: '35%'}
export const step = {
  margin: 0
}
export const stepBtn = {
  height: 48,
  width: 48
}
export const stepText = {
  textAlign: 'center'
}
