import 'babel-polyfill'
import React from 'react'
import { Provider } from 'react-redux'
import { render } from 'react-dom'
import { Router, Route, IndexRedirect, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import injectTapEventPlugin from 'react-tap-event-plugin'

import ClientList   from './containers/ClientList'
import ClientInfo   from './containers/ClientInfo'
import ClientNew    from './containers/NewClient'
import ClientWaist  from './containers/ClientWaist'

import configureStore from './store'
import { setAuthEnvironment } from './actions/app'
import { clearRoute, isAuthenticated } from './utils/'
import AuthService from './utils/AuthService'
import {
  ROUTE_LIST,
  ROUTE_NEW,
  ROUTE_INFO,
  ROUTE_WAIST,
} from './constants/routes'
import {
  AUTH0_DOMAIN,
  AUTH0_CLIENT_ID
} from './constants/'

import './style/app.css'

// if(module.hot) {
//   console.clear()
//   module.hot.accept()
// }

try { injectTapEventPlugin() } catch(ignore) { }

const store = configureStore()
const history = syncHistoryWithStore(browserHistory, store)

const auth = new AuthService(AUTH0_CLIENT_ID, AUTH0_DOMAIN, store.dispatch)
store.dispatch(setAuthEnvironment(auth))

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path='/' name='System'>
        <IndexRedirect to={ROUTE_LIST} />
        <Route path={ROUTE_LIST}  name='List'   component={ClientList}  onEnter={checkAuth}/>
        <Route path={ROUTE_NEW}   name='New'    component={ClientNew}   onEnter={checkAuth}/>
        <Route path={ROUTE_INFO}  name='Info'   component={ClientInfo}  onEnter={checkAuth}/>
        <Route path={ROUTE_WAIST} name='Waist'  component={ClientWaist} onEnter={checkAuth}/>
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)

function checkAuth(nextState, replace) {
  if (!isAuthenticated(auth.getToken())) {
    if (auth.loggedIn()) auth.logout()
    if (clearRoute(nextState.location.pathname) !== ROUTE_LIST)
      replace(ROUTE_LIST)
    auth.showWidget()
  }
  else if (!store.getState().rootReducer.app.isAuthenticated)
      auth.login()
}

function checkAuthNData(nextState, replace) {
  if (!Object.keys(store.getState().rootReducer.clients.client).length)
    replace(ROUTE_LIST)
  checkAuth(nextState, replace)
}
