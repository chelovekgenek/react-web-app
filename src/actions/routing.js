import {
  REDIRECT
} from '../constants'

export function redirect(url) {
  return {
    type: REDIRECT,
    payload: url
  }
}
