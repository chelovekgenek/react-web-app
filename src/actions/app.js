import {
  APP_SET_AUTH_ENVIRONMENT,
  APP_COMPLETE_LOGIN,
  APP_COMPLETE_LOGOUT
} from '../constants/'

export function setAuthEnvironment(payload) {
  return {
    type: APP_SET_AUTH_ENVIRONMENT,
    payload
  }
}
export function completeLogin() {
  return {
    type: APP_COMPLETE_LOGIN,
  }
}
export function completeLogout() {
  return {
    type: APP_COMPLETE_LOGOUT,
  }
}
