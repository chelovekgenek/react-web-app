import {
  PROMISE,
  JSON_SERVER_HOST_DB,
  JSON_SERVER_HOST_CLIENTS,
  CLIENTS_LOAD_ALL_REQUEST,
  CLIENTS_LOAD_ALL_SUCCESS,
  CLIENTS_LOAD_ALL_ERROR,
  CLIENTS_INSERT_NEW_REQUEST,
  CLIENTS_INSERT_NEW_SUCCESS,
  CLIENTS_INSERT_NEW_ERROR,
  CLIENTS_UPDATE_NEW_REQUEST,
  CLIENTS_UPDATE_NEW_SUCCESS,
  CLIENTS_UPDATE_NEW_ERROR,
  CLIENTS_SELECT,
  CLIENTS_CLEAR
} from '../constants/'

export function loadAll(token) {
  const options = {
    method: 'GET',
    headers: { authorization: 'Bearer ' + token}
  }
  return {
    type: PROMISE,
    actions: [
      CLIENTS_LOAD_ALL_REQUEST,
      CLIENTS_LOAD_ALL_SUCCESS,
      CLIENTS_LOAD_ALL_ERROR
    ],
    promise: fetch(JSON_SERVER_HOST_CLIENTS, options).then(response => response.json())
  }
}
export function insert(data, token, nextUrl) {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      authorization: 'Bearer ' + token
    },
    body: JSON.stringify(data)
  }
  return {
    type: PROMISE,
    actions: [
      CLIENTS_INSERT_NEW_REQUEST,
      CLIENTS_INSERT_NEW_SUCCESS,
      CLIENTS_INSERT_NEW_ERROR
    ],
    promise: fetch(JSON_SERVER_HOST_CLIENTS, options).then(response => response.json()),
    nextUrl
  }
}
export function update(id, data, token, nextUrl) {
  const options = {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      authorization: 'Bearer ' + token
    },
    body: JSON.stringify(data)
  }
  return {
    type: PROMISE,
    actions: [
      CLIENTS_UPDATE_NEW_REQUEST,
      CLIENTS_UPDATE_NEW_SUCCESS,
      CLIENTS_UPDATE_NEW_ERROR
    ],
    promise: fetch(JSON_SERVER_HOST_CLIENTS + '/' + id, options).then(response => response.json()),
    nextUrl
  }
}
export function select(id, nextUrl) {
  return {
    type: CLIENTS_SELECT,
    payload: id,
    nextUrl
  }
}
export function clear() {
  return {
    type: CLIENTS_CLEAR,
  }
}
