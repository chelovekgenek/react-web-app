import { push } from 'react-router-redux'
import { loadAll, clear } from '../actions/clients'

import {
  PROMISE,
  REDIRECT,
  CLIENTS_SELECT,
  APP_COMPLETE_LOGIN,
  APP_COMPLETE_LOGOUT
} from '../constants/'
import {
  ROUTE_LIST,
} from '../constants/routes'

const middleware = store => next => action => {
  switch (action.type) {
    case PROMISE:
      var [actionStart, actionSuccess, actionFailure] = action.actions
      store.dispatch({
        type: actionStart,
      })
      action.promise.then(data => {
        store.dispatch({
          type: actionSuccess,
          payload: data
        })
      }, error => store.dispatch({
        type: actionFailure,
        payload: error
      }))
      if (action.nextUrl) store.dispatch(push(action.nextUrl))
      break

    case REDIRECT:
      store.dispatch(push(action.payload))
      break

    case APP_COMPLETE_LOGIN:
      store.dispatch(loadAll(store.getState().rootReducer.app.auth.getToken()))
      return next(action)

    case APP_COMPLETE_LOGOUT:
      store.dispatch(clear())
      store.dispatch(push(ROUTE_LIST))
      return next(action)

    case CLIENTS_SELECT:
      if (action.nextUrl) store.dispatch(push(action.nextUrl))
      return next(action)

    default:
      return next(action)
  }
}

export default middleware
