import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Row } from 'react-grid-system'
import * as Style from '../constants/style'

import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import DatePicker from 'material-ui/DatePicker'
import Checkbox from 'material-ui/Checkbox'
import Divider from 'material-ui/Divider'
import DropDownMenu from 'material-ui/DropDownMenu'
import MenuItem from 'material-ui/MenuItem'
import NavigationChevronLeft from 'material-ui/svg-icons/navigation/chevron-left'
import {
  Step,
  Stepper,
  StepButton,
} from 'material-ui/Stepper'

import { STATUSES } from '../constants/'
import { ROUTE_LIST, ROUTE_WAIST } from '../constants/routes'
import { getStatusMenuItems, getWaistSteps } from '../utils'
import { update } from '../actions/clients'
import { redirect } from '../actions/routing'

class Info extends Component {
  state = {
    haventEmail: false,
    givenName: this.props.client.givenName    || '',
    familyName: this.props.client.familyName  || '',
    prefixName: this.props.client.prefixName  || '',
    birthDate: new Date(this.props.client.birthDate),
    dietician: this.props.client.dietician,
    status: this.props.client.status
  }

  save() {
    let data = {}
    if (this.state.givenName !== this.props.client.givenName)
      data.givenName = this.state.givenName
    if (this.state.familyName !== this.props.client.familyName)
      data.familyName = this.state.familyName
    if (this.state.prefixName !== this.props.client.prefixName)
      data.prefixName = this.state.prefixName
    if (this.state.status !== this.props.client.status)
      data.status = this.state.status
    if (this.state.dietician !== this.props.client.dietician && this.props.haventEmail)
      data.dietician = this.state.dietician
    if (this.state.birthDate.toISOString().slice(0, 10) !== this.props.client.birthDate)
      data.birthDate = this.state.birthDate.toISOString().slice(0, 10)
    if (Object.keys(data).length)
      this.props.update(this.props.client.id, data, this.props.auth.getToken(), ROUTE_LIST)
    else
      this.props.redirect(ROUTE_LIST)

    this.props.update
  }

  render() {
    return (
      <div className='content'>
        <div className='app'>
          <Row style={Style.row}>
            <div className='app-top-panel-left'>
              <FlatButton
                label='Back'
                icon={<NavigationChevronLeft />}
                onClick={() => this.props.redirect(ROUTE_LIST)}
              />
            </div>
            <div className='app-top-panel-right'>
              <FlatButton
                label='Remove'
                onClick={() => this.props.redirect(ROUTE_LIST)}
              />
            </div>
          </Row>

          <br />
          <Divider />


          <Row style={Style.row}>
            <div style={Style.left}>
              <Row style={Style.rowStep}>
                <Stepper linear={false}>
                  { getWaistSteps(this.props.client.waist) }
                </Stepper>
              </Row>
              <p style={Style.stepText}>Waist circumference</p>
            </div>
            <div style={Style.right}>
              <Row style={Style.rowStep}>
                <Stepper linear={false}>
                  <Step style={Style.step} completed={true}>
                    <StepButton className='step-icon' />
                  </Step>
                  <Step style={Style.step} active={true}>
                    <StepButton className='step-icon' />
                  </Step>
                  <Step style={Style.step} completed={false}>
                    <StepButton className='step-icon' />
                  </Step>
                  <Step style={Style.step} completed={false}>
                    <StepButton className='step-icon' />
                  </Step>
                </Stepper>
              </Row>
              <p style={Style.stepText}>Questionnairies</p>
            </div>
          </Row>

          <Divider style={{width: '100%'}}/>
          <br />

          <Row style={Style.rowTitle}>
            <span style={Style.textCommon}>CLIENT IDENTITY</span>
          </Row>
          <Row style={Style.row}>
            <TextField id='info-input-initials'
              style={Style.input}
              hintText='Initials*'
              value={this.state.givenName}
              onChange={e => this.setState({givenName: e.target.value})}
            />
            <TextField id='info-input-prefix'
              style={Style.input}
              hintText='Prefix'
              value={this.state.prefixName}
              onChange={e => this.setState({prefixName: e.target.value})}
            />
            <TextField id='info-input-surname'
              style={Style.input}
              hintText='Surname*'
              value={this.state.familyName}
              onChange={e => this.setState({familyName: e.target.value})}
            />
          </Row>
          <Row style={Style.row}>
            <DatePicker style={Style.input}
              hintText='Date of birth'
              container='inline'
              mode='landscape'
              value={this.state.birthDate}
              onChange={(arg, birthDate) => this.setState({birthDate})}
            />
          </Row>

          <br />
          <div className='app-row' style={{display: 'inline-block'}}>
            <div style={{...Style.left, width: '35%'}}>
              <Row style={Style.rowTitle}>
                <span style={Style.textCommon}>CLIENT CONTACT</span>
              </Row>
              <Row style={Style.row}>
                <TextField id='info-input-email'
                  style={Style.input}
                  disabled={this.state.haventEmail}
                  hintText='Email'
                  value={this.state.dietician}
                  onChange={e => this.setState({dietician: e.target.value})}
                />
              </Row>
              <Row style={Style.row}>
                <Checkbox
                  style={{marginLeft: 20, marginTop: 10}}
                  label='Client does not own email address'
                  onCheck={() => this.setState({haventEmail: !this.state.haventEmail})}
                />
              </Row>

              <br />

              <Row style={Style.rowTitle}>
                <span style={Style.textCommon}>PROCESS STATUS</span>
              </Row>
              <Row style={Style.row}>
                <DropDownMenu
                  autoWidth={false}
                  style={{width: 200}}
                  value={this.state.status}
                  onChange={(e, key, status) => this.setState({status})}
                >
                  { getStatusMenuItems() }
                </DropDownMenu>
              </Row>
            </div>
            <div style={{...Style.right, width: '65%'}}>
              <Row style={Style.rowTitle}>
                <span style={Style.textCommon}>FIELD NOTES</span>
              </Row>
              <Row style={Style.row}>
                <TextField id='info-input-notes'
                  style={{...Style.input, paddingTop: 6, width: '97%'}}
                  multiLine={true}
                  rowsMax={7}
                />
              </Row>
            </div>
          </div>

          <br />

          <Row style={Style.row}>
            <FlatButton
              style={{width: '20%', marginTop: 10, float: 'right'}}
              backgroundColor={Style.textNumber.color}
              hoverColor={'#6DA15F'}
              label='Save'
              onClick={::this.save}
            />
          </Row>

        </div>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    auth: state.rootReducer.app.auth,
    client: state.rootReducer.clients.client
  }
}
function mapDispatchToProps(dispatch) {
  return {
    redirect: bindActionCreators(redirect, dispatch),
    update: bindActionCreators(update, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Info)
