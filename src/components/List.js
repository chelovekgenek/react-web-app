import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router'

import { Row } from 'react-grid-system'

import Badge from 'material-ui/Badge'
import FlatButton from 'material-ui/FlatButton'
import IconButton from 'material-ui/IconButton'
import DropDownMenu from 'material-ui/DropDownMenu'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import Divider from 'material-ui/Divider'
import {
  Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn
} from 'material-ui/Table'
import ActionList from 'material-ui/svg-icons/action/list'
import ContentCreate from 'material-ui/svg-icons/content/create'
import ContentMessage from 'material-ui/svg-icons/communication/message'

import SearchField from './common/SearchField'

import { getStatusMenuItems, isDatesEqual, isSubstring, isEmpty } from '../utils'
import { STATUSES } from '../constants/'
import { ROUTE_NEW, ROUTE_INFO, ROUTE_WAIST } from '../constants/routes'
import { redirect } from '../actions/routing'
import { loadAll, select } from '../actions/clients'

const rowStyle = {
  margin: 0,
  paddingTop: '10px',
  paddingBottom: '10px'
}
const array = [
  {id: 0, name: 'John Smith',        birthDate: '15616516', status: '++'},
  {id: 1, name: 'Randal White',      birthDate: '1211432',  status:  '-+'},
  {id: 2, name: 'Stephanie Sanders', birthDate: '15616516', status: '++'},
  {id: 3, name: 'Steve Brown',       birthDate: '15132122', status: '+-'},
  {id: 4, name: 'John Smith',        birthDate: '54512145', status:  '--'},
  {id: 5, name: 'Stephanie Sanders', birthDate: '15616516', status: '++'},
]

const tableStyle = {
  headName:   {width: '17%'},
  headDate:   {width: '14.5%'},
  headParam:  {width: '31%',},
  headStatus: {paddingLeft: '45px'},
  colName:    {width: '17%'},
  colDate:    {width: '13%'},
  colParam:   {width: '32%'},
}

class List extends Component {

  state = {
    filterDate: {},
    filterName: ''
  }

  search(value) {
    if (!value)
      return this.setState({filterDate: {}, filterName: ''})
    let date
    try {
      date = new Date(value)
    } catch(ignore) { }
    if (!date || date.toString() === 'Invalid Date')
      this.setState({filterName: value})
    else
      this.setState({filterDate: date})
  }

  render() {
    let data = []
    if (this.props.clients.data) {
      data = this.props.clients.data
    }
    return (
      <div className='content'>
        <div className='app'>
          <Row style={{margin: 0}}>
            <div className='app-top-panel-left'>
              <FlatButton
                backgroundColor='rgb(88, 197, 111)'
                hoverColor='rgb(67, 155, 88)'
                labelStyle={{color: 'white'}}
                label='New client'
                primary={true}
                onClick={() => this.props.redirect(ROUTE_NEW)}
              />
            </div>
            <div className='app-top-panel-right'>
              <SearchField
                value={this.state.searchValue}
                search={::this.search}
              />
              <DropDownMenu value={1} style={{marginLeft: 50}}>
                <MenuItem value={1} primaryText='Last added' />
                <MenuItem value={2} primaryText='Last opened' />
              </DropDownMenu>
            </div>
          </Row>
          <Row style={rowStyle}>
            <Divider />
          </Row>
          <Row style={rowStyle}>
            <Table
              selectable={false}
            >
              <TableHeader
                adjustForCheckbox={false}
                displaySelectAll={false}
              >
                <TableRow>
                  <TableHeaderColumn style={tableStyle.headName}>
                    Name
                  </TableHeaderColumn>
                  <TableHeaderColumn style={tableStyle.headDate}>
                    Date of birth
                  </TableHeaderColumn>
                  <TableHeaderColumn style={tableStyle.headParam}>
                    Parameters
                  </TableHeaderColumn>
                  <TableHeaderColumn style={tableStyle.headStatus}>
                    Status
                  </TableHeaderColumn>
                  <TableHeaderColumn></TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody
                displayRowCheckbox={false}
                showRowHover={true}
              >
                { data.length && this.getTableContent(data, 999) }
                dafagsdhd
              </TableBody>
            </Table>
          </Row>
        </div>
      </div>
    )
  }
  getTableContent(data, limit) {
    let rows = []
    data.forEach((item, i) => {
      if ((i < limit && !this.state.filterName && isEmpty(this.state.filterDate))
      ||  (i < limit && !isEmpty(this.state.filterDate) && isDatesEqual(item.birthDate,  this.state.filterDate))
      ||  (i < limit && this.state.filterName && isSubstring(`${item.givenName} ${item.familyName}`, this.state.filterName)))
        try {
          rows.push(this.getTableRow(item))
        }
        catch(ignore) { }
    })
    return rows
  }
  getTableRow(item) {
    return (
      <TableRow key={item.id}>
        <TableRowColumn style={tableStyle.colName}>
          <a className='link' onClick={() => this.props.select(item.id, ROUTE_INFO)}>
            { `${item.givenName} ${item.familyName}` }
          </a>
        </TableRowColumn>
        <TableRowColumn style={tableStyle.colDate}>{item.birthDate}</TableRowColumn>
        <TableRowColumn style={tableStyle.colParam}>
          <FlatButton
            onClick={() => this.props.select(item.id, ROUTE_WAIST)}
            label={'Waist circumference | ' + Object.keys(item.waist).length}
          />
          <FlatButton
            onClick={() => console.log('some action')}
            label='Questionnaire | 1'
          />
        </TableRowColumn>
        <TableRowColumn>
          <DropDownMenu value={item.status} onChange={this.handleChange}>
            { getStatusMenuItems() }
          </DropDownMenu>
        </TableRowColumn>
        <TableRowColumn>
          <IconMenu
            iconButtonElement={<IconButton><ActionList hoverColor='rgb(67, 155, 88)'/></IconButton>}
            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
            targetOrigin={{horizontal: 'left', vertical: 'top'}}
          >
            <MenuItem
              primaryText='Edit client'
              leftIcon={<ContentCreate />}
              onClick={() => this.props.select(item.id, ROUTE_INFO)}
            />
            <MenuItem primaryText='Send question' leftIcon={<ContentMessage />} />
          </IconMenu>
        </TableRowColumn>
      </TableRow>
    )
  }
}

function mapStateToProps(state) {
  return {
    clients: state.rootReducer.clients
  }
}
function mapDispatchToProps(dispatch) {
  return {
    redirect: bindActionCreators(redirect, dispatch),
    select: bindActionCreators(select, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(List)
