import React, { Component } from 'react'

import IconButton from 'material-ui/IconButton'
import TextField from 'material-ui/TextField'

import NavigationClose from 'material-ui/svg-icons/navigation/close'
import ActionSearch from 'material-ui/svg-icons/action/search'

const styleTextField = {
  paddingTop: 8,
  verticalAlign: 'top',
  //paddingRight: (!this.state.value ? 48 : 0)
}
const styleBtn = {
  padding: 0,
  verticalAlign: 'bottom'
}

export default class SearchField extends Component {
  state = {
    value: ''
  }
  onTextChange(e) {
    let value = e.target.value
    this.setState({value})
  }
  onBtnClick() {
    this.props.search(this.state.value)
  }
  render() {
    return (
      <div style={{display: 'inline-block', verticalAlign: 'top'}}>
        <TextField
          hintText='Search by name and/or date of birth'
          style={styleTextField}
          value={this.state.value}
          onChange={::this.onTextChange}
        />
        <IconButton style={styleBtn} onClick={::this.onBtnClick}>
          <ActionSearch />
        </IconButton>
      </div>
    )
  }
}

/*<IconButton style={styleBtn} onClick={() => this.setState({value: ''})}>
  <NavigationClose />
</IconButton>*/
