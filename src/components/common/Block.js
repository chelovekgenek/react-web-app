import React, { Component } from 'react'

import { Row } from 'react-grid-system'
import * as Style from '../../constants/style'

import ActionDone from 'material-ui/svg-icons/action/done'

import DatePicker from 'material-ui/DatePicker'
import TextField from 'material-ui/TextField'

import {STATUS_COMPLETED, STATUS_ACTIVE} from '../../constants/'

import '../../style/block.css'

const defaultBlockStyle = {
  marginTop: 30,
  width: 550,
  height: 175,
  borderStyle: 'solid',
  borderWidth: 'thick',
  fontFamily:'Roboto Medium, Roboto',
  fontWeight: 500,
  fontSize: 18,
}

const defaultContentStyle = {
  position: 'absolute',
  paddingTop: 25
}

const meetingStyle = {
  ...defaultContentStyle,
  paddingLeft: 80
}
const dateStyle = {
  ...defaultContentStyle,
  paddingLeft: 250
}
const doneStyle = {
  ...defaultContentStyle,
  paddingLeft: 400,
}
const svgDoneSize = 85
const svgStyle = {
  width: svgDoneSize,
  height: svgDoneSize,
  color: 'white'
}

export default class Block extends Component {
  state = {
    value: this.props.value,
    date: this.props.date
  }
  handleValueChange(e) {
    let value = e.target.value
    if (value.length < 4)
      this.setState({value: value})
    else
      e.target.value = value.slice(0, 3)
    this.props.setParentValue(e.target.value)
  }
  handleDateChange(arg, date) {
    this.setState({date})
    this.props.setParentDate(date)
  }
  render() {
    const status = this.props.status
    let style = {}, className = '', completed = false, unavailable = false
    switch (this.props.status) {
      case STATUS_COMPLETED:
        style = {
          ...defaultBlockStyle,
          backgroundColor: Style.textNumber.color,
          borderColor: Style.textNumber.color,
          color: 'white'
        }
        className = 'block-input'
        completed = true
        break
      case STATUS_ACTIVE:
        style = {
          ...defaultBlockStyle,
          backgroundColor: 'white',
          borderColor: Style.textNumber.color
        }
        break
      default:
        style = {
          ...defaultBlockStyle,
          backgroundColor: 'rgba(248, 248, 248, 1)',
          borderColor: 'rgba(210, 200, 185, 1)'
        }
        unavailable = true

    }
    return (
      <div style={style}>
        <Row style={{...Style.row, display: 'inline-block'}}>
          <div style={meetingStyle}>
            <p style={{marginBottom: 0}}>MEASUREMENT</p>
            <div>
              {unavailable ? 'UNAVAILABLE' :
                <div style={{display: 'inline-block'}} className={className}>
                  <TextField id={'wc-' + this.props.status}
                    disabled={completed}
                    style={{width: 48, float: 'left', zIndex: 10000}}
                    value={this.state.value}
                    onChange={::this.handleValueChange}
                  />
                <p style={{float: 'right', marginTop: 13}}>cm</p>
                </div>
              }
            </div>
          </div>
            {unavailable || <div style={dateStyle} className={className}>
              <p style={{marginBottom: 0}}>DATE</p>
              <div>
                <DatePicker
                  textFieldStyle={{width: 128}}
                  hintText='Date'
                  container='inline'
                  mode='landscape'
                  value={this.state.date}
                  onChange={::this.handleDateChange}
                />
              </div>
            </div>}
          {completed && <div style={doneStyle}><ActionDone style={svgStyle}/></div>}
        </Row>
      </div>
    )
  }
}
