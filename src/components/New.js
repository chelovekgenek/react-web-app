import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Row } from 'react-grid-system'
import * as Style from '../constants/style'

import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import DatePicker from 'material-ui/DatePicker'
import Checkbox from 'material-ui/Checkbox'
import DropDownMenu from 'material-ui/DropDownMenu'
import MenuItem from 'material-ui/MenuItem'
import NavigationChevronLeft from 'material-ui/svg-icons/navigation/chevron-left'

import { ROUTE_LIST } from '../constants/routes'
import { insert } from '../actions/clients'
import { redirect } from '../actions/routing'

class New extends Component {
  state = {
    haventEmail: false,
    givenName: '',
    familyName: '',
    prefixName: '',
    dietician: '',
    birthDate: new Date(),
  }

  create() {
    let data = {}
    data.created    = Math.floor(Date.now() / 1000)
    data.birthDate  = this.state.birthDate.toISOString().slice(0,10)
    data.givenName  = this.state.givenName
    data.familyName = this.state.familyName
    data.prefixName = this.state.prefixName
    data.status     = 'active'
    data.comment    = ''
    data.waist      = {}
    if (!this.state.haventEmail) data.dietician = this.state.dietician

    this.props.insert(data, this.props.auth.getToken(), ROUTE_LIST)
  }

  render() {
    return (
      <div className='content'>
        <div className='app'>
          <Row style={Style.row}>
            <div className='app-top-panel-left'>
              <FlatButton
                label='Back'
                icon={<NavigationChevronLeft />}
                onClick={() => this.props.redirect(ROUTE_LIST)}
              />
            </div>
          </Row>

          <br />

          <Row style={Style.rowTitle}>
            <span style={Style.textNumber}>1. </span>
            <span style={Style.textCommon}>CLIENT IDENTITY</span>
          </Row>
          <Row style={Style.row}>
            <TextField style={Style.input}
              hintText='Initials*'
              value={this.state.givenName}
              onChange={e => this.setState({givenName: e.target.value})}
            />
            <TextField style={Style.input}
              hintText='Prefix'
              value={this.state.prefixName}
              onChange={e => this.setState({prefixName: e.target.value})}
            />
            <TextField style={Style.input}
              hintText='Surname*'
              value={this.state.familyName}
              onChange={e => this.setState({familyName: e.target.value})}
            />
          </Row>
          <Row style={Style.row}>
            <DatePicker style={Style.input}
              hintText='Date of birth'
              container='inline'
              mode='landscape'
              value={this.state.birthDate}
              onChange={(arg, birthDate) => this.setState({birthDate})}
            />
          </Row>

          <br />

          <Row style={Style.rowTitle}>
            <span style={Style.textNumber}>2. </span>
            <span style={Style.textCommon}>CLIENT CONTACT</span>
          </Row>
          <Row style={Style.row}>
            <TextField style={Style.input}
              disabled={this.state.haventEmail}
              hintText='Email'
              value={this.state.dietician}
              onChange={e => this.setState({dietician: e.target.value})}
            />
          </Row>
          <Row style={Style.row}>
            <Checkbox
              style={{marginLeft: 20, marginTop: 10}}
              label='Client does not own email address'
              onCheck={() => this.setState({haventEmail: !this.state.haventEmail})}
            />
          </Row>

          <br />

          <Row style={Style.rowTitle}>
            <span style={Style.textNumber}>2. </span>
            <span style={Style.textCommon}>START DATE PROCESS</span>
          </Row>
          <Row style={Style.row}>
            <DropDownMenu
              value={0}
              onChange={this.handleChange}
              autoWidth={false}
              style={{width: 200}}
            >
              <MenuItem value={0} primaryText='Immediately' />
              <MenuItem value={1} primaryText='Enter yourself' />
            </DropDownMenu>
          </Row>

          <br /><br />

          <Row style={Style.row}>
            <FlatButton
              style={{width: '20%'}}
              backgroundColor={Style.textNumber.color}
              hoverColor={'#6DA15F'}
              label='Create client'
              onClick={::this.create}
            />
          </Row>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.rootReducer.app.auth
  }
}
function mapDispatchToProps(dispatch) {
  return {
    redirect: bindActionCreators(redirect, dispatch),
    insert: bindActionCreators(insert, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(New)
