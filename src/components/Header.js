import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Breadcrumbs  from 'react-breadcrumbs'

import FlatButton from 'material-ui/FlatButton'
import FontIcon from 'material-ui/FontIcon'
import Power from 'material-ui/svg-icons/action/power-settings-new'

import '../style/header.css'

class Header extends Component {
  logout() {
    this.props.auth.logout()
    this.forceUpdate()
  }
  render() {
    const auth = this.props.auth, isAuthenticated = this.props.isAuthenticated
    return (
      <div className='header'>
        <div className='content'>
          <div className='breadcrumps'>
          {this.props.routes && <Breadcrumbs
            routes={this.props.routes}
            separator=' / '
          />}
          </div>
          <div className='header-power-btn'>
            <FlatButton
              backgroundColor='#f0eded'
              label={!isAuthenticated ? 'Login' : 'Logout'}
              labelPosition='before'
              primary={true}
              icon={<Power />}
              onClick={!isAuthenticated ? ::auth.showWidget : ::auth.logout}
            />
          </div>
        </div>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    auth: state.rootReducer.app.auth,
    isAuthenticated: state.rootReducer.app.isAuthenticated
  }
}
function mapDispatchToProps(dispatch) {
  return {
    // redirect: bindActionCreators(redirect, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header)
