import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Row } from 'react-grid-system'
import * as Style from '../constants/style'

import FlatButton from 'material-ui/FlatButton'
import Divider from 'material-ui/Divider'
import TextField from 'material-ui/TextField'
import Popover, {PopoverAnimationVertical} from 'material-ui/Popover'
import NavigationChevronLeft from 'material-ui/svg-icons/navigation/chevron-left'
import NavigationArrowDown from 'material-ui/svg-icons/navigation/arrow-drop-down'

import Block from './common/Block'

import { ROUTE_LIST, ROUTE_INFO } from '../constants/routes'
import { update } from '../actions/clients'
import { redirect } from '../actions/routing'

import {STATUS_COMPLETED, STATUS_ACTIVE} from '../constants/'

class Waist extends Component {
  state = {
    isPopoverOpen: false,
    blocks: [],
    activeBlockValue: '',
    activeBlockDate: new Date()
  }
  update() {
    const value = this.state.activeBlockValue
    const timestamp  = this.state.activeBlockDate.getTime()
    if (!value || isNaN(timestamp)) return

    let data = this.props.client
    data.waist[Math.floor(timestamp / 1000)] = value
    this.props.update(this.props.client.id, data, this.props.auth.getToken())
  }
  componentWillMount() {
    this.generateBlocks()
  }

  showPopover(e) {
    e.preventDefault()

    this.setState({
      isPopoverOpen: true,
      targetPopover: e.currentTarget
    })
  }
  closePopover(e) {
    this.setState({
      isPopoverOpen: false
    })
  }
  render() {
    const blocks = this.generateBlocks()
    return (
      <div className='content'>
        <div className='app'>
          <Row style={{...Style.row, paddingTop: 20}}>
            <div style={Style.left}>
              <FlatButton
                label='Back'
                icon={<NavigationChevronLeft />}
                onClick={() => this.props.redirect(ROUTE_INFO)}
              />
            </div>
            <div style={Style.right}>
              <FlatButton
                label='Procedure'
                icon={<NavigationArrowDown />}
                onClick={::this.showPopover}
              />
              <Popover
                open={this.state.isPopoverOpen}
                anchorEl={this.state.targetPopover}
                anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                onRequestClose={::this.closePopover}
                animation={PopoverAnimationVertical}
              >
                <div style={{width: 600}}>
                  <div style={Style.text}>
                    <p>Procedure middelomtrek</p>
                    <p />
                    <p>Het wordt geadviseerd om uzelf aan te leren volgens deze procedure de middelomtrek te meten. Mocht u dit niet doen kunt u melden wat u anders heeft gedaan in het notitieveld zodat u elke keer op dezelfde manier kunt meten</p>
                    <p />
                    <ul>
                      <li>Vraag de cliënt dikke kledingstukken uit te doen</li>
                      <li>Vraag aan cliënt ontspannen rechtop te gaan staan en normaal te ademen</li>
                      <li>Voel in de zij de onderkant van de onderste rib en markeer zo nodig de ondergrens recht onder de oksel met een huidpotlood of speld</li>
                      <li>Voel de bovenkant van het bekken en markeer zo nodig de bovengrens</li>
                      <li>Ga voor de cliënt staan en leg het meetlint horizontaal, losjes aansluitend, midden tussen de 2 gemarkeerde punten</li>
                      <li>Lees de waarde af in cm na een normale uitademing</li>
                      <li>Noteer de middelomtrek in het registratieveld</li>
                    </ul>
                  </div>
                </div>
             </Popover>
            </div>
          </Row>
          <Row style={Style.row}>
            <div style={{...Style.left, paddingLeft: 20}}>
              { blocks[0] }
            </div>
            <div style={{...Style.right, paddingRight: 20}}>
              { blocks[1] }
            </div>
          </Row>
          <Row style={Style.row}>
            <div style={{...Style.left, paddingLeft: 20}}>
              { blocks[2] }
            </div>
            <div style={{...Style.right, paddingRight: 20}}>
              { blocks[3] }
            </div>
          </Row>

          <br /><br />

          <Divider />

          <br />

          <Row style={Style.rowTitle}>
            <span style={Style.textCommon}>FIELD NOTES</span>
          </Row>
          <Row style={Style.row}>
            <TextField id='wc-field-notes'
              style={{...Style.input, width: '98%'}}
              multiLine={true}
              rowsMax={10}
            />
          </Row>
          <Row style={Style.row}>
            <div style={Style.right}>
              <FlatButton
                style={{width: 200}}
                backgroundColor={Style.textNumber.color}
                hoverColor={'#6DA15F'}
                disabled={this.state.countCompleted === 4}
                label='Save'
                onClick={::this.update}
              />
            </div>
          </Row>

        </div>
      </div>
    )
  }
  generateBlocks() {
    let blocks = [], countCompleted = 0
    for(let key in this.props.client.waist) {
      blocks.push(
        <Block
          status={STATUS_COMPLETED}
          value={this.props.client.waist[key]}
          date={new Date(parseInt(key) * 1000)}
        />
      )
      countCompleted++
    }
    if (blocks.length < 4)
      blocks.push(
        <Block
          status={STATUS_ACTIVE}
          value={this.state.activeBlockValue}
          date={this.state.activeBlockDate}
          setParentValue={value => this.setState({activeBlockValue: value})}
          setParentDate={date => this.setState({activeBlockValue: date})}
        />
      )
    for (let i = blocks.length; blocks.length < 4; i++) {
      blocks.push(
        <Block />
      )
    }
    return blocks
  }
}

function mapStateToProps(state) {
  return {
    auth: state.rootReducer.app.auth,
    client: state.rootReducer.clients.client
  }
}
function mapDispatchToProps(dispatch) {
  return {
    update: bindActionCreators(update, dispatch),
    redirect: bindActionCreators(redirect, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Waist)
