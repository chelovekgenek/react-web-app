import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Row } from 'react-grid-system'
import * as Style from '../constants/style'
import { getWaistSteps } from '../utils'

import {Tabs, Tab} from 'material-ui/Tabs'
import {
  Step,
  Stepper,
  StepButton,
} from 'material-ui/Stepper'

import * as routes from '../constants/routes'
import { STATUSES_FULL } from '../constants/'
import { redirect } from '../actions/routing'

import '../style/top-panel.css'

class TopPanel extends Component {
  state = {
    selectedFilter: STATUSES_FULL[0].value
  }
  getFilters() {
    let tabs = []
    STATUSES_FULL.forEach((item, i) => tabs.push(
      <Tab className='filter' label={item.label} value={item.value} key={i} />
    ))
    return (
      <Tabs
        className='filters'
        contentContainerClassName='filters-container'
        inkBarStyle={{marginTop:'-5px', height: '5px', backgroundColor: 'rgb(88, 197, 111)'}}
        value={this.state.selectedFilter}
        onChange={selectedFilter => this.setState({selectedFilter})}
      >
        { tabs }
      </Tabs>
    )
  }
  getSteps() {
    return(
      <div style={Style.right}>
        <Row style={Style.rowStep}>
          <Stepper linear={false}>
            { getWaistSteps(this.props.client.waist) }
          </Stepper>
        </Row>
      </div>
    )
  }

  render() {
    let contentLeft, contentRight
    switch (this.props.currentRoute.replace('/', '')) {
      case routes.ROUTE_LIST:
        contentLeft = 'Client list'
        contentRight = this.getFilters()
        break
      case routes.ROUTE_INFO:
        contentLeft = 'Client - '
                    + this.props.client.givenName
                    + ' '
                    + this.props.client.familyName
        break
      case routes.ROUTE_NEW:
        contentLeft = 'New client'
        break
      case routes.ROUTE_WAIST:
        contentLeft = 'Waist circumference'
        contentRight = this.getSteps()
        break
      default:
        contentLeft = 'Some title'

    }
    return (
      <div className='top-panel'>
        <div className='content'>
          <div className='title'>
            { contentLeft }
          </div>
          { contentRight }
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    client: state.rootReducer.clients.client,
    currentRoute: state.routing.locationBeforeTransitions.pathname
  }
}
function mapDispatchToProps(dispatch) {
  return {
    redirect: bindActionCreators(redirect, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TopPanel)
